const getSum = (str1, str2) => {

  if (typeof (str1) !== 'string' || typeof (str2) !== 'string') {
    return false;
  }

  var mem = 0;
  var maxLength = Math.max(str1.length, str2.length);

  var res = '';

  for (let i = 0; i < maxLength; i++) {
    var curNum1 = 0;
    var curNum2 = 0;
    if (i < str1.length) {
      curNum1 = +str1[i];
      if (isNaN(curNum1)) {
        return false;
      }
    }
    if (i < str2.length) {
      curNum2 = +str2[i];

      if (isNaN(curNum2)) {
        return false;
      }
    }
    var sum = curNum1 + curNum2 + mem;

    var toAdd = sum % 10;
    mem = (sum - toAdd) / 10;
    res += toAdd;
  }
  return res;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var postNumber = 0;
  var commentNumber = 0;

  for (var post of listOfPosts) {
    if (post.author === authorName) {
      postNumber++;
    }
    if (post.comments) {
      for (var comment of post.comments) {
        if (comment.author === authorName) {
          commentNumber++;
        }
      }
    }
  }
  return `Post:${postNumber},comments:${commentNumber}`;
};

const tickets = (people) => {
  var totalSum = 0;
  const price = 25;
  const cacheMachine = {
    25: 0,
    50: 0,
    100: 0
  };

  for (var p of people) {
    var change = p - price;
    if (change > totalSum) {
      return 'NO';
    }

    while (change > 0) {
      var hasChange = false;

      if (change % 100 === 0 && cacheMachine[100] > 0 && change > 0) {
        cacheMachine[100]--;
        change -= 100;
        hasChange = true;
      }

      if (change % 50 === 0 && cacheMachine[50] > 0 && change > 0) {
        cacheMachine[50]--;
        change -= 50;
        hasChange = true;
      }

      if (change % 25 === 0 && cacheMachine[25] > 0 && change > 0) {
        cacheMachine[25]--;
        change -= 25;
        hasChange = true;
      }

      if (!hasChange && change > 0) {
        return 'NO';
      }
    }

    cacheMachine[p]++;
    totalSum += price;

  }
  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
